<?php
	ini_set('upload_max_filesize', '100M');  
    ini_set('post_max_size', '100M');  
    ini_set('max_input_time', 300);  
    ini_set('max_execution_time', 300); 
	$counter = count($_FILES['fileField']['name']); 
	if($counter > 0){
		for($i = 0;$i < $counter;$i++)
		{
			$thisdir = getcwd();
			if(false == file_exists($_POST['d_path']))
			{
				if(mkdir($thisdir."/".$_POST['d_path'],0777,true)){}
				else{echo "failed to create";}
			}
			$target_path =  $_POST['d_path'] .basename( $_FILES['fileField']['name'][$i]);
		
			if(move_uploaded_file($_FILES['fileField']['tmp_name'][$i],$target_path)) {
				thumbnil_convertor($target_path);
				header("location: index.php");
			} 
			else
			{
				echo "There was an error uploading the file, please try again!";
			}
		}
	//	header("Location: index.php");
	}
	function thumbnil_convertor($file_path){
		$thumb_path = $_REQUEST['dest'];
		$real_path = $_REQUEST['d_path'];
		list($width,$height) = getimagesize($file_path);
   		$new_width = $_REQUEST['width'];
  		$new_height = $_REQUEST['height'];
		if($width > $height)
		{
			$new_height = floor( $height * ($new_width / $width ) );	
		}
		else
		{
			$new_width = floor( $width * ($new_height / $height ) );
		}
		
		$file_name =  basename($file_path);
		$info = pathinfo($file_name);
		$file_name =  basename($file_name,'.'.$info['extension']);	
	
		$thisdir = getcwd();
		if(false == file_exists($real_path))
		{
			if(mkdir($thisdir."/".$real_path,0777,true)){}
			else{echo "failed to create";}
		}
	
		$thisdir = getcwd();
		if(false == file_exists($thumb_path))
		{
			if(mkdir($thisdir."/".$thumb_path,0777,true)){}
			else{echo "failed to create";}
		}
		
		$tmpfile = $thumb_path . $file_name . "." . $info['extension'];
		if(!file_exists($tmpfile)){
			switch($info['extension']){
				case 'png' :$img = imagecreatefrompng($file_path);
							$tmp_img = imagecreatetruecolor( $new_width, $new_height );
							imagepng($img,$real_path.$file_name.".png");
							imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
							imagepng($tmp_img,$thumb_path.$file_name.".png");    
							break;
				
				case 'gif' : $img = imagecreatefromgif($file_path);
							$tmp_img = imagecreatetruecolor( $new_width, $new_height );
							imagegif($img,$real_path.$file_name.".gif");
							imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
							imagegif($tmp_img,$thumb_path.$file_name.".gif");    
							break;
							
				case 'jpeg': $img = imagecreatefromjpeg($file_path);
							 $tmp_img = imagecreatetruecolor( $new_width, $new_height );
							 imagejpeg($img,$real_path.$file_name.".jpeg");
							 imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
							 imagejpeg($tmp_img,$thumb_path.$file_name.".jpeg");    	
							 break;
									
				case 'jpg' : $img = imagecreatefromjpeg($file_path);
							 $tmp_img = imagecreatetruecolor( $new_width, $new_height );
							 imagejpeg($img,$real_path.$file_name.".jpg");
							 imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
							 imagejpeg($tmp_img,$thumb_path.$file_name.".jpg");    	
							 break;
							
			}
		}
		else
		{
			echo "File Already Exits";	
		}
	}
		
?>