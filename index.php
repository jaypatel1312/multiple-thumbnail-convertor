<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Multiple Thumbnil Creator</title>
<script type="text/javascript" language="javascript" src="js/jquery-1.5.1.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<script type="text/javascript">
	function converter(){
		if(document.getElementById('thumb_convert').checked	== true){
			document.getElementById('thumb_info').style.display = "block";
		}
		else
		{
			document.getElementById('thumb_info').style.display = "none";
		}
	}
	function showUser()
	{
		var filename = $("#fileField").val();
		alert(filename);
		
		$.ajax(
		{
			url : "show_selected_file.php",
			enctype: "multipart/form-data",
		
			data : {"files" : filename},
			type : "POST",
			success : function(n)
			{
				$('#show_info').html(n);
			}
			
		});
	}
</script>
</head>

<body>
<form enctype="multipart/form-data" action="uploaded.php" method="POST">
    ImagePath : <input type="file" id="fileField" name="fileField[]" multiple="multiple" onchange="showUser()" /><br /><br />
    <input type="checkbox" value="" id="thumb_convert" onclick="converter()"/>  Convert into Thumbnil <br /><br />
    <div id="thumb_info">
        Destination Path : <input type="text" name="d_path" /> (e.g  :  images/big/  )<br /><br />
        Thumbnil Height : <input type="text" name="height" /><br /><br />
        Thumbnil Width : <input type="text" name="width" /><br /><br />
        Thumbnil Path : <input type="text" name="dest" />   (e.g  :  images/small/  )<br /><br />
    </div>
  	<input type="submit" name="Upload" id="Upload" value="Add Image" /><br />
    
   <div id="show_info">
   		
   </div>
</form>
</body>
</html>
